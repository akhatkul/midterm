package kz.aitu.midterm.repository;

import kz.aitu.midterm.entity.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {

}
