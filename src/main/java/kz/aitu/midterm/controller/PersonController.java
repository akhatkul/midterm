package kz.aitu.midterm.controller;

import kz.aitu.midterm.entity.Person;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import kz.aitu.midterm.service.PersonService;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(personService.getAll());
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deleteById(@PathVariable long id) {
        personService.deleteById(id);
    }
    @PostMapping("api/v2/users")
    public ResponseEntity<?>createNewPerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.create(person));
    }

}

