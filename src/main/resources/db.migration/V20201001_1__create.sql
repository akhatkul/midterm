CREATE TABLE  if not exists Person
(
    id INT,
    firstName VARCHAR 255),
    lastName VARCHAR(255),
    phone VARCHAR(255),
    city VARCHAR(255),
    telegram VARCHAR(255),
    PRIMARY KEY (ID)
);
